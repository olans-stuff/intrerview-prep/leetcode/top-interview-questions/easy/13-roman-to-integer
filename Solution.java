import java.util.HashMap;

class Solution {
    public int romanToInt(String s) {
        //declare A hashmap and have the roman numerals indicating values with key
        HashMap<Character, Integer> romanNums = new HashMap<>();
        //Enter our values in for numerals and put
        romanNums.put('I', 1);
        romanNums.put('V', 5);
        romanNums.put('X', 10);
        romanNums.put('L', 50);
        romanNums.put('C', 100);
        romanNums.put('D', 500);
        romanNums.put('M', 1000);

        //Now Lets say input is IIV, output = 7 as I + I + v = 7
        // need also work wat from back as has to be in right order.
        int n = s.length();
        int output = 0;
        int prev = 0;
        for (int i = n - 1; i >= 0; i--) {
            char currentCharInS = s.charAt(i);
            int currentvalCorrespondeing = romanNums.get(currentCharInS);

            if (currentvalCorrespondeing >= prev) {
                output += currentvalCorrespondeing;
            } else {
                output -= currentvalCorrespondeing;
            }

            prev = currentvalCorrespondeing;
        }
        return output;
         /*
         IIV = 117
         7 >= 0
         output = 7
         prev = 7

         1 <= 7
         Output = 6
         prev = 1

         1 >- 1
         Output = 7
          */
    }
}